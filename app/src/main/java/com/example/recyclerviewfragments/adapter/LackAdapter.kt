package com.example.recyclerviewfragments.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewfragments.R
import com.example.recyclerviewfragments.databinding.ItemRecyclerviewBinding
import com.example.recyclerviewfragments.model.Lack
import com.example.recyclerviewfragments.OnClickListener

class LackAdapter(private val lacks: MutableList<Lack>, private val listener: OnClickListener):
    RecyclerView.Adapter<LackAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lack = lacks[position]
        with(holder){
            setListener(lack)
            binding.nom.text = lack.name
            binding.modul.text = lack.module
            binding.data.text = lack.date
            binding.justificada.isChecked = lack.checkbox
        }
    }

    override fun getItemCount(): Int {
        return lacks.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemRecyclerviewBinding.bind(view)

        fun setListener(lack: Lack){
            binding.root.setOnClickListener {
                listener.onClick(lack)
            }
        }
    }
}