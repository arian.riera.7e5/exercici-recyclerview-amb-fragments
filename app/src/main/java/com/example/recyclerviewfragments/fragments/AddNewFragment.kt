package com.example.recyclerviewfragments.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.recyclerviewfragments.R
import com.example.recyclerviewfragments.databinding.FragmentAddNewBinding
import com.example.recyclerviewfragments.model.Lack
import com.example.recyclerviewfragments.model.LackViewModel

class AddNewFragment : Fragment() {


    lateinit var binding: FragmentAddNewBinding
    private val model: LackViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAddNewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.add.setOnClickListener {
            val newLack = Lack(binding.nameValue.text.toString(), binding.spinner.selectedItem.toString(), binding.time.text.toString(), binding.check.isChecked)
            model.addLack(newLack)
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, DialogFragment())
                commit()
            }
        }
    }

}