package com.example.recyclerviewfragments.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewfragments.OnClickListener
import com.example.recyclerviewfragments.adapter.LackAdapter
import com.example.recyclerviewfragments.databinding.FragmentRvBinding
import com.example.recyclerviewfragments.model.LackViewModel
import androidx.lifecycle.Observer
import com.example.recyclerviewfragments.R
import com.example.recyclerviewfragments.model.Lack

class DialogFragment : Fragment(), OnClickListener{

    private lateinit var lackAdapter: LackAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRvBinding
    private val model: LackViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRvBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lackAdapter = LackAdapter(model.lackList.value!!, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = lackAdapter
        }

        model.lackList.observe(viewLifecycleOwner, Observer {
            lackAdapter.notifyDataSetChanged()
        })

        binding.lackAdd.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, AddNewFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }
    }

    override fun onClick(lack: Lack) {
        model.select(lack)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }

}