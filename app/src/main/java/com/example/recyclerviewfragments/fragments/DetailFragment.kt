package com.example.recyclerviewfragments.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.recyclerviewfragments.databinding.FragmentDetailBinding
import com.example.recyclerviewfragments.model.LackViewModel
import androidx.lifecycle.Observer

class DetailFragment : Fragment() {


    private val model: LackViewModel by activityViewModels()
    lateinit var binding: FragmentDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.selectedLack.observe(viewLifecycleOwner, Observer {
            val lack = it
            binding.name.text = lack.name
            binding.module.text = lack.module
            binding.date.text = lack.date
            binding.check.isChecked = lack.checkbox
        })
    }
}