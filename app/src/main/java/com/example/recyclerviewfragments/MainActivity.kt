package com.example.recyclerviewfragments

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.recyclerviewfragments.databinding.ActivityMainBinding
import com.example.recyclerviewfragments.fragments.DialogFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DialogFragment())
            commit()
        }

    }
}