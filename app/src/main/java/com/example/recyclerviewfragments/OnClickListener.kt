package com.example.recyclerviewfragments

import com.example.recyclerviewfragments.model.Lack

interface OnClickListener {
    fun onClick(lack: Lack)
}