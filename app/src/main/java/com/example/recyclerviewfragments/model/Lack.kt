package com.example.recyclerviewfragments.model

data class Lack(val name: String, var module: String, var date: String, var checkbox: Boolean)