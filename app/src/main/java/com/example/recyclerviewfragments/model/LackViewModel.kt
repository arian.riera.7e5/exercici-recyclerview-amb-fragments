package com.example.recyclerviewfragments.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LackViewModel: ViewModel(){
    var lackList = MutableLiveData<MutableList<Lack>>().apply {
        this.value = mutableListOf<Lack>(
            Lack("Joan", "BBD", " Wed, 12-Des, 00:55:12", true),
            Lack("Marc", "Acces a dades", " Mon, 12-Nov, 00:55:12", false),
            Lack("Lluc", "Programacio", " Sat, 12-Des, 00:55:12", true),
            Lack("Carles", "BBD", " Fri, 01-Feb, 00:55:12", true),
            Lack("Jordi", "FOL", " Wed, 18-Sep, 00:55:12", false)
        )
    }

    var selectedLack = MutableLiveData<Lack>()

    fun addLack(lack: Lack){
        lackList.value!!.add(lack)
        lackList.postValue(lackList.value)
    }

    fun select(lack: Lack) {
        selectedLack.postValue(lack)
        selectedLack.value = lack
    }
}